<?php

namespace TGApp\Account\Filter;

use TGApp\App\Filter\AbstractFilter;

/**
 *
 *
 * @author    Thomas Gnandt <Thomas_Gnandt@gmx.net>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Login extends AbstractFilter {

    protected function init() {
        $this->validate('email')->is('email')->setMessage('Email is invalid.');
        $this->validate('password')->is('strlenMin', 8)->setMessage('Password has to be at least 8 characters long.');
    }

}
