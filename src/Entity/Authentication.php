<?php

namespace TGApp\Account\Entity;

/**
 *
 *
 * @author    Thomas Gnandt <Thomas_Gnandt@gmx.net>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * @Entity @Table(name="account_authentication")
 */
class Authentication {

    /**
     * @Id
     * @Column(type="integer")
     */
    protected $user_id;

    /**
     * @Column(type="string")
     */
    protected $provider;

    /**
     * @Column(type="string")
     */
    protected $auth_id;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="authentications")
     */
    protected $user;
}
