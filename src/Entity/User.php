<?php

namespace TGApp\Account\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 *
 * @author    Thomas Gnandt <Thomas_Gnandt@gmx.net>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * @Entity @Table(name="account_user", uniqueConstraints={@UniqueConstraint(name="user_email", columns={"email"})})
 */
class User {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string")
     */
    protected $email;

    /**
     * @Column(type="string")
     */
    protected $password;

    /**
     * @OneToMany(targetEntity="Authentication", mappedBy="user")
     */
    protected $authentications;

    public function __construct() {
        $this->authentications = new ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setData($data) {
        foreach ($data as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

}
