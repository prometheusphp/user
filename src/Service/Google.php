<?php

namespace TGApp\Account\Service;

use Google_Client;

/**
 *
 *
 * @author    Thomas Gnandt <Thomas_Gnandt@gmx.net>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Google extends Google_Client {

    public function setRedirectUri($redirectUri) {
        $redirectUri = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $redirectUri;
        parent::setRedirectUri($redirectUri);
    }
}
