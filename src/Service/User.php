<?php

namespace TGApp\Account\Service;

use Doctrine\ORM\EntityManager;
use TGApp\App\Entity\Factory;
use TGApp\Account\Entity\User as UserEntity;
use Aura\Session\Session;

/**
 *
 *
 * @author    Thomas Gnandt <Thomas_Gnandt@gmx.net>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class User {

    protected $entityManager;
    protected $factory;
    protected $session;
    protected $user;

    public function __construct(EntityManager $entitiyManager, Factory $factory, Session $session) {
        $this->entityManager = $entitiyManager;
        $this->factory = $factory;
        $this->session = $session;
    }

    protected function init() {
        $segment = $this->getSession();
        $id = $segment->get('user_id');
        if ($id) {
            $this->user = $this->entityManager->find(UserEntity::class, $id);
            if (null === $this->user) {
                $segment->clear();
            }
        }
    }

    public function get($email) {
        return $this->entityManager->getRepository(UserEntity::class)->findOneBy(['email' => $email]);
    }

    public function verify(UserEntity $user, $password) {
        if (password_verify($password, $user->getPassword())) {
            if (password_needs_rehash($user->getPassword(), PASSWORD_DEFAULT)) {
                $user->setPassword($this->hash($password));
                $this->entityManager->flush();
            }
            return true;
        }
        return false;
    }

    public function save($data) {
        try {
            $data['password'] = $this->hash($data['password']);
            $user = $this->factory->__invoke(UserEntity::class);
            $user->setData($data);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            return $user;
        } catch (Exception $ex) {
            return false;
        }
    }

    public function hash($password) {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function login(UserEntity $user) {
        $this->getSession()->set('user_id', $user->getId());
        $this->user = $user;
        $this->session->regenerateId();
    }

    public function isLoggedIn() {
        $this->init();
        return $this->user !== null;
    }

    public function logout() {
        $this->getSession()->set('user_id', null);
        $this->user = null;
        $this->session->regenerateId();
    }

    protected function getSession() {
        return $this->session->getSegment(self::class);
    }
}
