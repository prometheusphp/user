<?php

namespace TGApp\Account;

use TGApp\App\Config\AbstractConfig;
use Aura\Di\Container;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

/**
 *
 *
 * @author    Thomas Gnandt <Thomas_Gnandt@gmx.net>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Config extends AbstractConfig {

    public function getConfig() {
        return [
            'di' => [
                'setters' => [
                    \TGApp\Account\Service\Google::class => [
                        'setAuthConfig' => \json_decode(file_get_contents('config/auth/google.json'), true),
                        'setScopes' => 'email'
                    ],
                ],
                'services' => [
                    'filter_factory' => \Aura\Filter\FilterFactory::class,
                    'user' => \TGApp\Account\Service\User::class,
                    \TGApp\Account\Middleware\LoginForbidden::class => \TGApp\Account\Middleware\LoginForbidden::class,
                    \TGApp\Account\Middleware\LoginRequired::class => \TGApp\Account\Middleware\LoginRequired::class,
                ],
                'types' => [
                    'services' => [
                        \Aura\Filter\FilterFactory::class => 'filter_factory',
                        EntityManager::class => EntityManager::class,
                        \TGApp\Account\Service\User::class => 'user',
                    ],
                ]
            ],
            'routes' => [
                \TGApp\App\RouteFactory::GROUPS => [
                    '/account' => [
                        \TGApp\App\RouteFactory::ACTIONS => [
                            '/' => [
                                'name' => 'account/index',
                                'controller' => \TGApp\Account\Controller\Account::class,
                                'action' => 'index',
                                'middleware' => [\TGApp\Account\Middleware\LoginRequired::class]
                            ],
                            '/register' => [
                                [
                                    'method' => ['GET'],
                                    'name' => 'account/register',
                                    'controller' => \TGApp\Account\Controller\Account::class,
                                    'action' => 'registerGet',
                                    'middleware' => [\TGApp\Account\Middleware\LoginForbidden::class]
                                ],
                                [
                                    'method' => ['POST'],
                                    'controller' => \TGApp\Account\Controller\Account::class,
                                    'action' => 'registerPost',
                                    'middleware' => [\TGApp\Account\Middleware\LoginForbidden::class]
                                ],
                            ],
                            '/login' => [
                                [
                                    'method' => ['GET'],
                                    'name' => 'account/login',
                                    'controller' => \TGApp\Account\Controller\Account::class,
                                    'action' => 'loginGet',
                                    'middleware' => [\TGApp\Account\Middleware\LoginForbidden::class]
                                ],
                                [
                                    'method' => ['POST'],
                                    'controller' => \TGApp\Account\Controller\Account::class,
                                    'action' => 'loginPost',
                                    'middleware' => [\TGApp\Account\Middleware\LoginForbidden::class]
                                ],
                            ]
                        ],
                        \TGApp\App\RouteFactory::GROUPS => [
                            '/auth_service' => [
                                \TGApp\App\RouteFactory::ACTIONS => [
                                    '/google' => [
                                        'method' => ['GET'],
                                        'name' => 'account/auth_service/google',
                                        'controller' => \TGApp\Account\Controller\AuthService::class,
                                        'action' => 'google',
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
            ],
            'doctrine' => [
                'entities' => [
                    './vendor/tgnandt/account/src/Entity'
                ]
            ],
        ];
    }

    /**
     *
     * Define params, setters, and services before the Container is locked.
     *
     * @param Container $di The DI container.
     *
     * @return null
     *
     */
    public function define(Container $di) {
        $di->setters[\TGApp\Account\Service\Google::class]['setRedirectUri'] = $di->lazy(function() use ($di) {
            return $di->get('router')->pathFor('account/auth_service/google');
        });
        if (!$di->has(EntityManager::class)) {
            $di->set(EntityManager::class, $di->lazy(function() use ($di) {
                        $config = $di->get('config')->doctrine->toArray();
                        $doctrineConfig = Setup::createAnnotationMetadataConfiguration($config['entities'], $config['devMode']);

                        return EntityManager::create($config['connection'], $doctrineConfig);
                    }));
        }
    }

}
