<?php

namespace TGApp\Account\Middleware;

use Slim\Router;
use TGApp\Account\Service\User;

/**
 *
 *
 * @author    Thomas Gnandt <Thomas_Gnandt@gmx.net>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
abstract class AbstractMiddleware {

    protected $router;
    protected $user;

    public function __construct(Router $router, User $user) {
        $this->router = $router;
        $this->user = $user;
    }
}
