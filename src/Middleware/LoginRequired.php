<?php

namespace TGApp\Account\Middleware;

use Psr\Http\Message\RequestInterface;
use Slim\Http\Response;

/**
 *
 *
 * @author    Thomas Gnandt <Thomas_Gnandt@gmx.net>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class LoginRequired extends AbstractMiddleware {

    public function __invoke(RequestInterface $request, Response $response, Callable $next) {
        if (!$this->user->isLoggedIn()) {
            return $response->withRedirect($this->router->pathFor('account/login'));
        }
        return $next($request, $response);
    }
}
