<?php

namespace TGApp\Account\Controller;

use Psr\Http\Message\RequestInterface;
use TGApp\Account\Service\User;
use TGApp\Account\Service\Google;
use Aura\Filter\FilterFactory;
use Slim\Router;
use Slim\Http\Response;
use Slim\Http\Request;

/**
 *
 *
 * @author    Thomas Gnandt <Thomas_Gnandt@gmx.net>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class AuthService {

    protected $user;
    protected $router;
    protected $google;
    protected $filterFactory;

    public function __construct(FilterFactory $filterFactory, User $user, Router $router, Google $google) {
        $this->user = $user;
        $this->router = $router;
        $this->google = $google;
        $this->filterFactory = $filterFactory;
    }

    public function google(Request $request, Response $response, $args = []) {
        if (!$request->getParam('code')) {
            return $response->withRedirect($this->router->pathFor('account/login'));
        }
        $token = $this->google->fetchAccessTokenWithAuthCode($request->getParam('code'));
        if (array_key_exists('error', $token)) {
            return $response->withRedirect($this->router->pathFor('account/login'));
        }
        $this->google->setAccessToken($token);
        $tokenData = $this->google->verifyIdToken();
        $id = $tokenData['sub'];
        error_log(print_r($tokenData, true));
        if (!array_key_exists('email', $tokenData)) {
            return $response->withRedirect($this->router->pathFor('account/login'));
        }
        $filter = $this->filterFactory->newValueFilter();
        if (!$filter->validate($tokenData['email'], 'email')) {
            return $response->withRedirect($this->router->pathFor('account/login'));
        }
        $user = $this->user->get($tokenData['email']);
        if (!$user) {
            $user = $this->user->save(['email' => $tokenData['email']]);
        }
        $this->user->login($user);
        return $response->withRedirect($this->router->pathFor('account/index'));
    }

}
