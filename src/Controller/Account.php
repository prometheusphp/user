<?php

namespace TGApp\Account\Controller;

use TGApp\App\Controller\AbstractController;
use Aura\Session\CsrfToken;
use Slim\Views\Twig;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Aura\Filter\FilterFactory;
use TGApp\Account\Filter\Register;
use TGApp\Account\Filter\Login;
use TGApp\Account\Service\User;
use TGApp\Account\Service\Google;
use Slim\Router;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 *
 *
 * @author    Thomas Gnandt <Thomas_Gnandt@gmx.net>
 * @copyright Copyright (c) 2017 Thomas Gnandt
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
class Account extends AbstractController {

    protected $csrf;
    protected $filterFactory;
    protected $user;
    protected $router;
    protected $google;

    public function __construct(Twig $view, CsrfToken $csrf, FilterFactory $filterFactory, User $user, Router $router, Google $google) {
        $this->view = $view;
        $this->csrf = $csrf;
        $this->filterFactory = $filterFactory;
        $this->user = $user;
        $this->router = $router;
        $this->google = $google;
    }

    public function registerGet(RequestInterface $request, Response $response, $args = []) {
        $values = array_merge([
            'form_key' => $this->csrf->getValue(),
            'google_url' => $this->google->createAuthUrl()
        ], $args);
        return $this->view->render($response, 'account/register.twig', $values);
    }

    public function registerPost(RequestInterface $request, Response $response) {
        $data = $this->validatePost($request, Register::class);
        if (array_key_exists('errors', $data)) {
            return $this->registerGet($request, $response, $data);
        }
        if ($this->user->get($data['email'])) {
            $data['errors']['email'] = 'An account with this email already exists.';
            return $this->registerGet($request, $response, $data);
        }
        $user = $this->user->save($data);
        if (!$user) {
            $data['errors']['email'] = 'Could not create the account. An error occured.';
            return $this->registerGet($request, $response, $data);
        }
        $this->user->login($user);
        return $response->withRedirect($this->router->pathFor('account/index'));
    }

    public function index(RequestInterface $request, Response $response) {
        return $this->view->render($response, 'account/index.twig');
    }

    public function loginGet(RequestInterface $request, Response $response, $args = []) {
        $values = array_merge([
            'form_key' => $this->csrf->getValue(),
            'google_url' => $this->google->createAuthUrl()
        ], $args);
        return $this->view->render($response, 'account/login.twig', $values);
    }

    public function loginPost(RequestInterface $request, Response $response) {
        $data = $this->validatePost($request, Login::class);
        if (array_key_exists('errors', $data)) {
            return $this->loginGet($request, $response);
        }
        if ($user = $this->user->get($data['email'])) {
            if ($this->user->verify($user, $data['password'])) {
                $this->user->login($user);
                return $response->withRedirect($this->router->pathFor('account/index'));
            }
        }
        $data['errors']['email'] = 'Email or password are incorrect.';
        return $this->registerGet($request, $response, $data);
    }

    protected function validatePost(Request $request, $filterName) {
        $data = $request->getParsedBody();
        if (!isset($data['form_key']) || !$this->csrf->isValid($data['form_key'])) {
            $data['errors']['form_key'] = 'Page has expired. Please try again';
            return $data;
        }
        $filter = $this->filterFactory->newSubjectFilter($filterName);
        $data = $this->clean($data, $filter->getFieldNames());
        if (!$filter->apply($data)) {
            foreach ($filter->getFailures() as $field => $failures) {
                $data['errors'][$field] = $filter->getFailures()->getMessagesForFieldAsString($field);
            }
            return $data;
        }

        return $data;
    }

}
